<?php

use timurmelnikov\widgets\LoadingOverlayPjax;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Medicine */
/* @var $modelDetail app\models\MedicineDetail */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="medicine-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, "name_arabic")->textInput(["maxlength" => true]) ?>

    <?= $form->field($model, "name_english")->textInput(["maxlength" => true]) ?>

<!--    --><?//= $form->field($model, "type")->textInput(["maxlength" => true]) ?>

    <?= $form->field($model, "how_to_use")->textarea(["rows" => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t("app","Save"), ["class" => "btn btn-success" ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php
timurmelnikov\widgets\LoadingOverlayAsset::register($this);
?>

<?php

// JavaScript code (heredoc syntax)
$script =<<<JS

    // Settings (you can not use it, then it's all by default) 
    $.LoadingOverlaySetup ({ 
        color: "rgba (0, 0, 0, 0.4)", 
        maxSize: "80px", 
        minSize: "20px", 
        resizeInterval: 0, 
        size: "50%" 
    });

    // Overlay jQuery LoadingOverlay on the element with ID # p0, when sending the AJAX request 
    $(document).ajaxSend(function (event, jqxhr, settings) { 
        $.LoadingOverlay("show");
    });

    // Hide jQuery LoadingOverlay on the element with ID # p0, after executing the AJAX request 
    $(document).ajaxComplete(function(event, jqxhr, settings) {
        $.LoadingOverlay("hide");
    });

JS;

// Connect the script in the view
$this->registerJs($script , yii\web\View::POS_READY);

?>