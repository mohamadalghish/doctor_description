<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
use kartik\export\ExportMenu;
use  timurmelnikov\widgets\LoadingOverlayPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReceiptSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Receipts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="receipt-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app','Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?php
    $gridColumns = [
        'id',
        [
            'attribute' => 'date',
            'value' => 'date',
            'format' => 'raw',
            'filter' => DatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'date',
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                ]
            ])
        ],
        'patient_name',
        [
            'label' => Yii::t('app','Medicines'),
            'value' => function($model){
                return join(', ', yii\helpers\ArrayHelper::map($model->receiptMedicines, 'id', 'name_english'));
            },

        ],


        ['class' => 'yii\grid\ActionColumn','template'=>'{view} {delete}'],
    ];

    // Renders a export dropdown menu
    echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns
    ]);
    ?>


    <?php  LoadingOverlayPjax::begin ([
    'color' =>  'rgba(102, 255, 204, 0.2)' ,
    'fontawesome'  =>  'fa fa-spinner fa-spin'
    ]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]); ?>

    <?php LoadingOverlayPjax::end(); ?>


</div>